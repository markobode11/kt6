import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(6, 9);
        System.out.println(g.getCenterAsString());
    }

    /**
     * Vertex class to define vertices for Graph class.
     */
    class Vertex {

        /**
         * ID of the vertex that should be unique.
         */
        private final String id;

        /**
         * Next Vertex that is connected to this Vertex.
         */
        private Vertex next;

        /**
         * Arc that is between next Vertex and this Vertex.
         */
        private Arc first;

        /**
         * Info field to help different methods operations.
         */
        private int info = 0;

        /**
         * Eccentricity of the vertex.
         * If 0 then no eccentricity is defined yet.
         */
        private int eccentricity;

        /**
         * Constructor for Vertex class.
         *
         * @param s id for the Vertex
         * @param v first Vertex connected to this Vertex
         * @param e Arc between the two vertices
         */
        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        /**
         * Second constructor to construct Vertex with only id.
         *
         * @param s id for the Vertex
         */
        Vertex(String s) {
            this(s, null, null);
        }

        /**
         * String representation of the Vertex.
         *
         * @return the id of the Vertex
         */
        @Override
        public String toString() {
            return id;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        /**
         * ID of the Arc that should be unique.
         */
        private final String id;

        /**
         * The destination of the Arc
         */
        private Vertex target;

        /**
         * Next Arc for this Arc
         */
        private Arc next;

        /**
         * Constructor for Arc
         *
         * @param s id of the Arc
         * @param v target of this Arc
         * @param a next Arc for this Arc
         */
        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        /**
         * Second constructor to construct Arc with only id.
         *
         * @param s id of the Arc
         */
        Arc(String s) {
            this(s, null, null);
        }

        /**
         * String representation of the Arc.
         *
         * @return id of the Arc
         */
        @Override
        public String toString() {
            return id;
        }
    }


    /**
     * Graph consists of Vertices and Arcs.
     */
    class Graph {

        /**
         * ID of the Graph which should be unique.
         */
        private final String id;

        /**
         * First Vertex of this Graph.
         */
        private Vertex first;

        /**
         * List of all the Vertices that are present in this Graph.
         */
        private final List<Vertex> vertices = new ArrayList<>();

        /**
         * Constructor for Graph.
         *
         * @param s id for the Graph
         * @param v first Vertex of this graph
         */
        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        /**
         * Second constructor to construct the Graph with only id.
         *
         * @param s id for the Graph
         */
        Graph(String s) {
            this(s, null);
        }

        /**
         * String representation of the Graph beginning with the id of the Graph.
         * Then followed by each Vertex in this Graph and each Vertex`s Arcs.
         * If Arc does not have a direction then the Arc is represented twice- from A to B and then from B to A.
         *
         * @return graph as a string
         */
        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuilder sb = new StringBuilder(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        /**
         * Create new Vertex for this Graph.
         *
         * @param vid new Vertex`s id
         * @return created Vertex
         */
        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        /**
         * Create new Arc for this Graph.
         *
         * @param aid  new Arc`s id
         * @param from Arc`s start point
         * @param to   Arc`s destination
         */
        public void createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + (n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            int info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vertices.add(v);
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges

            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Create a distance matrix of this graph.
         *
         * @param adjMatrix adjacency matrix of the graph
         * @return distance matrix of the graph
         */
        public int[][] getDistMatrix(int[][] adjMatrix) {
            int vertexes = this.vertices.size(); // number of vertices

            int[][] result = new int[adjMatrix.length][adjMatrix.length];
            if (vertexes < 1) return result;
            int noPath = 2 * vertexes + 1; // something impossible
            for (int i = 0; i < vertexes; i++) {
                for (int j = 0; j < vertexes; j++) {
                    if (i == j) {
                        result[i][j] = 0;
                    }
                    if (adjMatrix[i][j] == 0) {
                        result[i][j] = noPath; // no path between vertices i and j
                    } else {
                        result[i][j] = 1; // value is the length of the arc
                    }
                }
            }
            return result;
        }

        /**
         * Floyd-Warshall`s algorithm to get the shortest paths from each vertex to any other vertex.
         *
         * @return distance matrix where each distance is as short as possible
         */
        public int[][] getShortestPaths(int[][] distMatrix) {
            // Algorithm`s base taken from https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
            // Modified this and getDistMatrix() to fit my requirements.

            int n = this.vertices.size(); // number of vertices

            for (int k = 0; k < n; k++) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        int newLength = distMatrix[i][k] + distMatrix[k][j];
                        if (distMatrix[i][j] > newLength) {
                            distMatrix[i][j] = newLength; // new path is shorter
                        }
                    }
                }
            }
            return distMatrix;
        }

        /**
         * Find the distance between given vertex and the vertex furthest from the given vertex.
         *
         * @param vertex     the vertex which eccentricity is wanted
         * @param distMatrix graphs distance matrix
         * @return eccentricity of the vertex
         */
        public int getEccentricity(Vertex vertex, int[][] distMatrix) {
            int index = vertices.indexOf(vertex);
            int eccentricity = 0;
            for (int i = 0; i < distMatrix.length; i++) {
                if (distMatrix[index][i] > eccentricity) {
                    eccentricity = distMatrix[index][i];
                }

            }
            return eccentricity;
        }

        /**
         * Find the center of this graph. Center is the vertex which eccentricity is the smallest.
         * If several vertices share the smallest eccentricity then all these vertices are a part of the center.
         *
         * @return center of the graph as a list of vertices
         */
        public List<Vertex> getCenter() {
            List<Vertex> center = new ArrayList<>();

            if (vertices.size() < 1) {
                return center;
            } else if (vertices.size() == 1) {
                center.add(vertices.get(0));
                return center;
            }

            int[][] adjMatrix = createAdjMatrix();
            int[][] distMatrix = getDistMatrix(adjMatrix);
            int[][] distMatrixShortest = getShortestPaths(distMatrix);

            for (Vertex vertex : vertices) {
                vertex.eccentricity = getEccentricity(vertex, distMatrixShortest);
                if (center.size() == 0) {
                    center.add(vertex);
                } else if (vertex.eccentricity < center.get(0).eccentricity) {
                    center = new ArrayList<>();
                    center.add(vertex);
                } else if (vertex.eccentricity == center.get(0).eccentricity) {
                    center.add(vertex);
                }
            }
            return center;
        }

        /**
         * Get the center of this Graph as a string.
         *
         * @return sentence to describe the center of this Graph
         */
        public String getCenterAsString() {
            List<Vertex> center = getCenter();
            String result = this.toString() + "\n";
            if (center.size() == 0) {
                result += "Graph with no vertices does not have a center!";
            } else if (center.size() == 1) {
                result += "The center of Graph " + id + "(shown above) is " + center.get(0);
            } else {
                StringBuilder vertices = new StringBuilder();
                for (Vertex vertex : center) {
                    if (center.indexOf(vertex) != center.size() - 1) {
                        vertices.append(vertex);
                        vertices.append(", ");

                    } else {
                        vertices.append(vertex);
                    }

                }
                result += "The center of Graph " + id + "(shown above) are vertices " + vertices.toString();
            }
            return result;
        }
    }
} 
